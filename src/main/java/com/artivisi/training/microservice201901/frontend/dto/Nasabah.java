package com.artivisi.training.microservice201901.frontend.dto;

import lombok.Data;

@Data
public class Nasabah {
    private String id;
    private String nomor;
    private String namaDepan;
    private String namaBelakang;
    private String email;
    private String noHp;
}
